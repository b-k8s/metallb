# MetalLB setup

* Install the k8s objects with `kustomize build beast | kubectl apply -f -`
* Apply the config changes to the USG from `./usg/usgconfig`
* Update the Unifi Controller's config and save the
  `./usg/config.gateway.json` on it to ensure the aforementioned config persists
  across reboots

See [test](https://gitlab.com/b-k8s/test) repo for various tests

## Note on Bitnami SealedSecret

Generated with:

```shell
openssl rand -base64 128 | \
  kubectl create secret generic -n metallb-system memberlist \
  --dry-run=client \
  --from-file=secretkey=/dev/stdin \
  -o yaml \
  --type Opaque | \
  kubeseal -o yaml | \
  grep -v creationTimestamp > base/sealedsecret.yaml
```
